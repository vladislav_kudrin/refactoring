package sequential.search;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Sequential searching {@code index} of {@code ELEMENT} in {@code array}.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/06/2020
 */
public class SequentialSearch {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        final int MINIMAL_RANDOM_VALUE = -25;
        final int MAXIMAL_RANDOM_VALUE = 51;
        final int ELEMENT = 7;
        int index;

        System.out.print("Введите размер массива: ");
        int[] array = new int[input.nextInt()];

        fillArray(array, MINIMAL_RANDOM_VALUE, MAXIMAL_RANDOM_VALUE);

        System.out.println("\n" + Arrays.toString(array) + "\n");

        index = findElementIndex(array, ELEMENT);

        System.out.println("\n" + ((index == -1) ? "Нет такого значения." : "Номер элемента в последовательности: " + index));
    }

    /**
     * Fills {@code array} with elements from {@code minimalRandomValue} to {@code maximalRandomValue}.
     *
     * @param array an one-dimensional array.
     */
    private static void fillArray(int[] array, int minimalRandomValue, int maximalRandomValue) {
        for(int index = 0; index < array.length; index++) {
            array[index] = minimalRandomValue + (int)(Math.random() * maximalRandomValue);
        }
    }

    /**
     * Sequential searching index of {@code element} in {@code array}.
     *
     * @param array an one-dimension array.
     * @param element a value of a desired element.
     * @return {@code index} or -1.
     */
    private static int findElementIndex(int[] array, int element) {
        for(int index = 0; index < array.length; index++) {
            if(array[index] == element) {
                return index;
            }
        }

        return -1;
    }
}