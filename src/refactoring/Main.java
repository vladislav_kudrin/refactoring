package refactoring;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Sequential searching {@code index} of {@code element} in {@code array}.
 *
 * @author Vladislav
 * @version 1.0
 * @since 03/28/2020
 */
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int index;

        System.out.print("Введите размер массива: ");
        int[] array = new int[input.nextInt()];

        fillArray(array);

        System.out.println("\n" + Arrays.toString(array) + "\n");

        System.out.print("Введите значение элемента: ");

        index = findElementIndex(array, input.nextInt());

        System.out.println("\n" + ((index == -1) ? "Нет такого значения." : "Номер элемента в последовательности: " + index));
    }

    /**
     * Fills {@code array} with elements from -25 to 25.
     *
     * @param array an one-dimensional array.
     */
    private static void fillArray(int[] array) {
        for(int index = 0; index < array.length; index++) {
            array[index] = -25 + (int)(Math.random() * 51);
        }
    }

    /**
     * Sequential searching index of {@code element} in {@code array}.
     *
     * @param array an one-dimension array.
     * @param element a value of a desired element.
     * @return {@code index} or -1.
     */
    private static int findElementIndex(int[] array, int element) {
        for(int index = 0; index < array.length; index++) {
            if(array[index] == element) {
                return index;
            }
        }

        return -1;
    }
}